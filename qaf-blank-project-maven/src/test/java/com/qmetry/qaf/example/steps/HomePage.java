package com.qmetry.qaf.example.steps;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

import static com.qmetry.qaf.automation.step.CommonStep.assertLinkWithTextPresent;
import static com.qmetry.qaf.automation.step.CommonStep.click;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage> implements
HomePageLocators  {
	
	
	 @FindBy(locator = POPULAR_LINK)
	    private QAFWebElement popularLinkText;
	 @FindBy(locator = RECIPES_LINK)
	    private QAFWebElement recipesLinkText;
	 @FindBy(locator = SPRING_COOKING_LINK)
	    private QAFWebElement springCookLinkText;

	public QAFWebElement getRecipesLinkText() {
		return recipesLinkText;
	}

	public QAFWebElement getSpringCookLinkText() {
		return springCookLinkText;
	}

	public QAFWebElement getPopularLinkText() {
		return popularLinkText;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		 driver.get("/");
			
	}
	
}
