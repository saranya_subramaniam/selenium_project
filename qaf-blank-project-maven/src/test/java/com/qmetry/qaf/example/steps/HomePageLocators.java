package com.qmetry.qaf.example.steps;

public interface HomePageLocators {
	static final String HEADER_LOC = "{'locator':'css=.header';'desc':'Header of Page'}";
    static final String MENU_LOC = "{'locator':'css=.menu_area';'desc':'Menu of Page'}";
    static final String SLIDER_LOC = "{'locator':'css=.nivoSlider';'desc':'Slid Show in Home Page'}";
    static final String SPRING_COOKING_LINK ="{\"locator\":\"xpath=//span[text()='SPRING COOKING']\",\"desc\":\"Spring cooking page Link\"}";
    static final String RECIPES_LINK = "{\"locator\":\"xpath=//span[text()='Recipes']\",\"desc\":\"Recipe page Link\"}";
    static final String POPULAR_LINK = " {\"locator\":\"xpath=//span[text()='POPULAR']\",\"desc\":\"Popular page Link\"}";
   
}
