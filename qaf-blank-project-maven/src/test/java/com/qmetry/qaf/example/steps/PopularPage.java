package com.qmetry.qaf.example.steps;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class PopularPage extends WebDriverBaseTestPage<HomePage> implements PopularPageLocators {

	

	@FindBy(locator = POPULAR_LINK)
	private List<QAFWebElement> popularHyperLinks;
	@FindBy(locator = RIBBON_HEADER)
	private QAFWebElement ribbonHeader;

	public List<QAFWebElement> getPopularHyperLinks() {
		return popularHyperLinks;
	}

	public QAFWebElement getRibbonHeader() {
		return ribbonHeader;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		parent.getPopularLinkText().click();
	}
	
	/**
	 * @param headerText
	 *            : headerText text in ribbon header
	 */
	@QAFTestStep(description = "return the header text")
	public String ribbonHeaderText() {
		String headerText=ribbonHeader.getText();
		return headerText;
	}

	/**
	 * 
	 * @return errorCount
	 * 					: return the broken link count
	 * @throws IOException
	 */
	@QAFTestStep(description = "return the broken link in the page")
	public int getbrokenLink() throws IOException {
		List<QAFWebElement> popularnonEmptyHyperLinks = new ArrayList<QAFWebElement>();
		for (QAFWebElement elementLink : popularHyperLinks) {
			String hrefAtt = elementLink.getAttribute("href");
			if (hrefAtt == null || hrefAtt.isEmpty() || hrefAtt.contains("javascript")) {
				continue;
			}
			popularnonEmptyHyperLinks.add(elementLink);
		}

		 int errorCount=10;
		int properResponse = 0;
		for (QAFWebElement nonEmptyLink : popularnonEmptyHyperLinks) {

			String url = nonEmptyLink.getAttribute("href");

			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.connect();
			String responseURL = connection.getResponseMessage();
			int reponseCode = connection.getResponseCode();
			if (reponseCode >= 400) {
				errorCount++;
			} else {
				properResponse++;
			}
			connection.disconnect();
			// System.out.println("Response for the URL "+url+" "+reponseCode+"
			// "+responseURL);
		}
		return errorCount;
	}

}
