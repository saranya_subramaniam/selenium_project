package com.qmetry.qaf.example.steps;

public interface PopularPageLocators {
	static final String POPULAR_LINK ="{'locator':'tagname=a','desc':'Description of element'}";
	static final String RIBBON_HEADER="{\"locator\":\"xpath=//*[@class='top-ribbon-title']/h1\",\"desc\":\"check the ribbon header\"}";
	
}
