package com.qmetry.qaf.example.steps;

import org.openqa.selenium.JavascriptExecutor;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.selenium.JavaScriptHelper;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class RecipesPage extends WebDriverBaseTestPage<HomePage> implements RecipesPageLocators{

	@FindBy(locator = VISIBLE_TEXT)
    private QAFWebElement visibleText;
	@FindBy(locator = RECOMMENDED_LIST)
    private QAFWebElement recommendedList;
	@FindBy (locator = HEALTHY_LINK_TEXT)
	private QAFWebElement healthy_link_text;
 
	public QAFWebElement getHealthy_link_text() {
		return healthy_link_text;
	}

	public QAFWebElement getVisibleText() {
		return visibleText;
	}

	public QAFWebElement getRecommendedList() {
		return recommendedList;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
		parent.getRecipesLinkText().click();
	}
	/**
	 * 
	 */
	@QAFTestStep(description = "scroll to the particular element")
	public void moveToRecommendList() {
		recommendedList.executeScript("scrollIntoView(true)");
		
	}

}
