package com.qmetry.qaf.example.steps;

public interface RecipesPageLocators {
	static final String HEALTHY_LINK_TEXT="{\"locator\":\"xpath= //*[text()='Healthy']\",\"desc\":\"check the Healthy Link\"}";
	static final String VISIBLE_TEXT ="{\"locator\":\"xpath= //*[@class='rib-text']\",\"desc\":\"check Recipe of the day title\"}";
	static final String RECOMMENDED_LIST ="{\"locator\":\"xpath=//*[@class='js-selected']\",\"desc\":\"Rcommended list Item\",\"scroll-options\": \"true\"}";

}
