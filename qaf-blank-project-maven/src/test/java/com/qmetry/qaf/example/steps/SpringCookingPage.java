package com.qmetry.qaf.example.steps;

import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;

import java.util.List;

import org.openqa.selenium.Keys;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.selenium.JavaScriptHelper;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class SpringCookingPage extends WebDriverBaseTestPage<HomePage> implements SpringCookingPageLocators {

	@FindBy(locator = SEE_ALL)
	private List<QAFWebElement> seeAllLinks;

	@FindBy(locator = FOOTER_SEARCH_INPUT)
	private QAFWebElement footerSearch;
	@FindBy(locator = RIBBON_HEADER)
	private QAFWebElement ribbonHeader;

	public QAFWebElement getRibbonHeader() {
		return ribbonHeader;
	}

	public QAFWebElement getFooterSearch() {
		return footerSearch;
	}

	public void setSeeAllLinks(List<QAFWebElement> seeAllLinks) {
		this.seeAllLinks = seeAllLinks;
	}

	public List<QAFWebElement> getSeeAllLinks() {
		return seeAllLinks;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		parent.getSpringCookLinkText().click();

	}
/**
 * 
 * @param searchTerm
 * 					:value need to search in the element
 */
	@QAFTestStep(description = "scroll to the particular element and enter the field")
	public void searchFor(String searchTerm) {
		footerSearch.executeScript("scrollIntoView(true)");
		footerSearch.sendKeys(searchTerm + Keys.ENTER);
	}

}
