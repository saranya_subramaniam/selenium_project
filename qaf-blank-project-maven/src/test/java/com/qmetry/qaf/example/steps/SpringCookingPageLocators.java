package com.qmetry.qaf.example.steps;

public interface SpringCookingPageLocators {
	static final String SPRING_COOKING_LINK ="{\"locator\":\"xpath=//span[text()='SPRING COOKING']\",\"desc\":\"Spring cooking page Link\"}";
	static final String FOOTER_SEARCH_INPUT = "{'locator':'id=footer-search-field';'desc':'get see all link'}";
	static final String SEE_ALL ="{\"locator\":\"xpath=//a[@class='btn btn-over-dark-alt']\",\"desc\":\"get see all link\",\"scroll-options\": \"true\"}";
	static final String RIBBON_HEADER="{\"locator\":\"xpath=//*[@class='top-ribbon-title']/h1\",\"desc\":\"check the ribbon header\"}";
}
