package com.qmetry.qaf.example.test;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.example.steps.PopularPage;
import static com.qmetry.qaf.automation.step.CommonStep.assertText;
import static org.testng.Assert.assertEquals;;


public class PopularPageTest extends WebDriverTestBase {

	
	@Test(priority = 0,description = "check the title of the page")
	public void testPageTiltle() {
		PopularPage popularPageObj=new PopularPage();
		popularPageObj.launchPage(null);
		String popTitle=popularPageObj.getTestBase().getDriver().getTitle();
		assertEquals(popTitle, "POPULAR RECEIPE");
	}
	
	@Test(priority = 1, description = "check the broken links",enabled = false)
	public void testBrokenLinks() throws IOException{
		PopularPage popularPageObj=new PopularPage();
		int countBrokenLink=popularPageObj.getbrokenLink();
		System.out.println("total broken link"+countBrokenLink);
		Assert.assertEquals(countBrokenLink, 0);
	}
	@Test(priority = 2, description = "check the Ribbon header of the page")
	public void testRibbonHeader() {
		PopularPage popularPageObj=new PopularPage();
		popularPageObj.launchPage(null);
		popularPageObj.getRibbonHeader().assertText("TOP-RATED RECIPES");
		//assertText(headerTxt,"Top-Rated Recipes");
	}
}
