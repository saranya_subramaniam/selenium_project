package com.qmetry.qaf.example.test;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.selenium.JavaScriptHelper;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.StringMatcher;
import com.qmetry.qaf.example.steps.RecipesPage;
import static com.qmetry.qaf.automation.step.CommonStep.assertLinkWithTextPresent;

public class RecipesPageTest extends WebDriverTestBase {
	RecipesPage recipeObj=new RecipesPage();
	@Test(priority = 1, description = "check the title of the page")
	public void testPageTitle() {
		
		recipeObj.launchPage(null);
		String recipeTitle=recipeObj.getTestBase().getDriver().getTitle();
		assertEquals(recipeTitle, "RECEIPE");
	}
	@Test(priority = 2, description = "check the Ribbon header of the page")
	public void testRibbonTextTitle() {
		recipeObj.launchPage(null);
		recipeObj.getVisibleText().assertText("RECIPE OF THE DAY");
		
	}
	@Test(priority = 3, description = "check the Link present in the RECOMMENDED LIST ")
	public void testRecommendedList() throws InterruptedException {
		recipeObj.launchPage(null);
		recipeObj.moveToRecommendList();
		
		recipeObj.getRecommendedList().click();
	
		assertLinkWithTextPresent("Healthy1");		
	}
	
}
