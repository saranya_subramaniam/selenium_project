package com.qmetry.qaf.example.test;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.selenium.JavaScriptHelper;
import com.qmetry.qaf.example.steps.PopularPage;
import com.qmetry.qaf.example.steps.SpringCookingPage;
import static com.qmetry.qaf.automation.step.CommonStep.assertEnabled;
import static com.qmetry.qaf.automation.step.CommonStep.assertText;


public class SpringCookingPageTest extends WebDriverTestBase{
	@Test(priority = 0,description = "check the title of the page")
	public void testPageTiltle() {
		SpringCookingPage springCookPageObj=new SpringCookingPage();	
		springCookPageObj.launchPage(null);
		String springCookTitle=springCookPageObj.getTestBase().getDriver().getTitle();
		//System.out.println("Title"+springCookTitle);
		assertEquals(springCookTitle, "SPRING COOK");
	}
	
	@Test(priority = 1, description = "check the SEE THEM ALL text is enabled")
	public void testSeeAllLinks() {
		SpringCookingPage springCookPageObj=new SpringCookingPage();	
		springCookPageObj.launchPage(null);	
		springCookPageObj.getSeeAllLinks().get(1).executeScript("scrollIntoView(true)");
		springCookPageObj.getSeeAllLinks().get(1).assertEnabled("SEE THEM ALL");
	}
	@Test(priority = 2, description = "check the foot search with desired result")
	public void testFooterSearch() {
		SpringCookingPage springCookPageObj=new SpringCookingPage();	
		springCookPageObj.launchPage(null);	
		
		springCookPageObj.searchFor("pancake");
	
		assertText("searchModuleTitle","3,481 RESULTS");
	}
	
	@Test(priority = 3, description = "check the Ribbon header of the page")
	public void testRibbonHeader() {
		SpringCookingPage springCookPageObj=new SpringCookingPage();	
		springCookPageObj.launchPage(null);	
		springCookPageObj.getRibbonHeader().assertText("SPRING");
		//assertText(headerTxt,"Top-Rated Recipes");
	}

}
